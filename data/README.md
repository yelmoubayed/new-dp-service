* **6-pipedream_summary.json** Latest version of Pipedream output before official release from Global Phasing.
* **2-autoproc_summary.json:** autoPROC output data (innerShell, outerShell, and overall are stored as objects).
* **dataset_registration.json:** datasets settings registration data.
