## Pipedream
* **1-pipedream_summary.json / 1-summary.out:** A single input model with a monomer in the asu. 1 ligand.
* **2-pipedream_summary.json / 2-summary.out:** A single input model with a homotrimer in the asu. 1 ligand but 3 binding sites (1 per chain).
* **3-pipedream_summary.json / 3-summary.out:** A single input model with a monomer in the asu. 2 input ligands
* **4-pipedream_summary.json / 4-summary.out:** 3 input models( all with a monomer in the asu. 1 ligand.
* **5-pipedream_summary.json:** A single input model with a monomer in the asu. 1 ligand (Added chain/residue name to solutions container).

## autoPROC
* **1-autoproc_summary.json:** autoPROC output data: innerShell, outerShell, and overall are stored as name-value pairs.
* **3-autoproc_summary.json:** autoPROC output data from Raphael's run on 26/08/2021.

