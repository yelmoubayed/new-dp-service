# Python imports
import json
import logging
import sys

# Django imports
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Models imports
from api.models.report_model import Report
from api.models.autoPROC_model import autoPROC
from api.models.dpstep_model import DPStep

# Activate logger for debugging
logger=logging.getLogger('dataproc')
# logger.info('TEMPLATE')

@csrf_exempt
def storeautoProcInput(request):

    """
    Parse JSON data and call store functions for each model
    """

    if request.method=='POST':
        json_data=json.loads(request.body)
        json_data_report=json_data

        try:
            report=storeParseReport(json_data_report)
            storeParseDPStep
            storeParseAutoPROC

            return JsonResponse({"STATUS": "INPUT SUCCESSFULLY REGISTERED"})
        
        except :
            return JsonResponse({"STATUS":"ERROR OCCURRED"}, safe=False)

@csrf_exempt
def storeParseReport(data):

    """
    Creates nodes and relationships for each report with relative properties
    """

    try:      
        autoproccontainer=data['AutoProcContainer']['AutoProc']
        autoprocscalingcontainer=data['AutoProcContainer']['AutoProcScalingContainer']['AutoProcScaling']
        autoprocscalingstatistics=data['AutoProcContainer']['AutoProcScalingContainer']['AutoProcScalingStatistics']

        report=Report(refinedCell_beta=autoproccontainer['refinedCell_beta'],
            refinedCell_b=autoproccontainer['refinedCell_b'],
            wavelength=autoproccontainer['wavelength'],
            refinedCell_a=autoproccontainer['refinedCell_a'],
            refinedCell_alpha=autoproccontainer['refinedCell_alpha'],
            spaceGroup=autoproccontainer['spaceGroup'],
            refinedCell_c=autoproccontainer['refinedCell_c'],
            refinedCell_gamma=autoproccontainer['refinedCell_gamma'],

            recordTimeStamp=autoprocscalingcontainer['recordTimeStamp'],
            resolutionEllipsoidAxis13=autoprocscalingcontainer['resolutionEllipsoidAxis13'],
            resolutionEllipsoidAxis33=autoprocscalingcontainer['resolutionEllipsoidAxis33'],
            resolutionEllipsoidAxis23=autoprocscalingcontainer['resolutionEllipsoidAxis23'],
            resolutionEllipsoidValue2=autoprocscalingcontainer['resolutionEllipsoidValue2'],
            resolutionEllipsoidAxis21=autoprocscalingcontainer['resolutionEllipsoidAxis21'],
            resolutionEllipsoidAxis31=autoprocscalingcontainer['resolutionEllipsoidAxis31'],
            resolutionEllipsoidAxis22=autoprocscalingcontainer['resolutionEllipsoidAxis22'],
            resolutionEllipsoidAxis32=autoprocscalingcontainer['resolutionEllipsoidAxis32'],
            resolutionEllipsoidValue3=autoprocscalingcontainer['resolutionEllipsoidValue3'],
            resolutionEllipsoidAxis12=autoprocscalingcontainer['resolutionEllipsoidAxis12'],
            resolutionEllipsoidValue1=autoprocscalingcontainer['resolutionEllipsoidValue1'],
            resolutionEllipsoidAxis11=autoprocscalingcontainer['resolutionEllipsoidAxis11'],

            innerShell_multiplicity=autoprocscalingstatistics['innerShell']['multiplicity'],
            innerShell_resolutionLimitLow=autoprocscalingstatistics['innerShell']['resolutionLimitLow'],
            innerShell_rMeasWithinIPlusIMinus=autoprocscalingstatistics['innerShell']['rMeasWithinIPlusIMinus'],
            innerShell_DanoOverSigDano=autoprocscalingstatistics['innerShell']['DanoOverSigDano'],
            innerShell_completenessSpherical=autoprocscalingstatistics['innerShell']['completenessSpherical'],
            innerShell_nTotalUniqueObservations=autoprocscalingstatistics['innerShell']['nTotalUniqueObservations'],
            innerShell_rMerge=autoprocscalingstatistics['innerShell']['rMerge'],
            innerShell_rPimWithinIPlusIMinus=autoprocscalingstatistics['innerShell']['rPimWithinIPlusIMinus'],
            innerShell_anomalousCompletenessEllipsoidal=autoprocscalingstatistics['innerShell']['anomalousCompletenessEllipsoidal'],
            innerShell_nTotalObservations=autoprocscalingstatistics['innerShell']['nTotalObservations'],
            innerShell_completenessEllipsoidal=autoprocscalingstatistics['innerShell']['completenessEllipsoidal'],
            innerShell_anomalousCompleteness=autoprocscalingstatistics['innerShell']['anomalousCompleteness'],
            innerShell_anomalousMultiplicity=autoprocscalingstatistics['innerShell']['anomalousMultiplicity'],
            innerShell_resolutionLimitHigh=autoprocscalingstatistics['innerShell']['resolutionLimitHigh'],
            innerShell_completeness=autoprocscalingstatistics['innerShell']['completeness'],
            innerShell_ccHalf=autoprocscalingstatistics['innerShell']['ccHalf'],
            innerShell_rPimAllIPlusIMinus=autoprocscalingstatistics['innerShell']['rPimAllIPlusIMinus'],
            innerShell_meanIOverSigI=autoprocscalingstatistics['innerShell']['meanIOverSigI'],
            innerShell_anomalousCompletenessSpherical=autoprocscalingstatistics['innerShell']['anomalousCompletenessSpherical'],
            innerShell_ccAnomalous=autoprocscalingstatistics['innerShell']['ccAnomalous'],
            innerShell_rMeasAllIPlusIMinus=autoprocscalingstatistics['innerShell']['rMeasAllIPlusIMinus'],

            outerShell_multiplicity=autoprocscalingstatistics['outerShell']['multiplicity'],
            outerShell_resolutionLimitLow=autoprocscalingstatistics['outerShell']['resolutionLimitLow'],
            outerShell_rMeasWithinIPlusIMinus=autoprocscalingstatistics['outerShell']['rMeasWithinIPlusIMinus'],
            outerShell_DanoOverSigDano=autoprocscalingstatistics['outerShell']['DanoOverSigDano'],
            outerShell_completenessSpherical=autoprocscalingstatistics['outerShell']['completenessSpherical'],
            outerShell_nTotalUniqueObservations=autoprocscalingstatistics['outerShell']['nTotalUniqueObservations'],
            outerShell_rMerge=autoprocscalingstatistics['outerShell']['rMerge'],
            outerShell_rPimWithinIPlusIMinus=autoprocscalingstatistics['outerShell']['rPimWithinIPlusIMinus'],
            outerShell_anomalousCompletenessEllipsoidal=autoprocscalingstatistics['outerShell']['anomalousCompletenessEllipsoidal'],
            outerShell_nTotalObservations=autoprocscalingstatistics['outerShell']['nTotalObservations'],
            outerShell_completenessEllipsoidal=autoprocscalingstatistics['outerShell']['completenessEllipsoidal'],
            outerShell_anomalousCompleteness=autoprocscalingstatistics['outerShell']['anomalousCompleteness'],
            outerShell_anomalousMultiplicity=autoprocscalingstatistics['outerShell']['anomalousMultiplicity'],
            outerShell_resolutionLimitHigh=autoprocscalingstatistics['outerShell']['resolutionLimitHigh'],
            outerShell_completeness=autoprocscalingstatistics['outerShell']['completeness'],
            outerShell_ccHalf=autoprocscalingstatistics['outerShell']['ccHalf'],
            outerShell_rPimAllIPlusIMinus=autoprocscalingstatistics['outerShell']['rPimAllIPlusIMinus'],
            outerShell_meanIOverSigI=autoprocscalingstatistics['outerShell']['meanIOverSigI'],
            outerShell_anomalousCompletenessSpherical=autoprocscalingstatistics['outerShell']['anomalousCompletenessSpherical'],
            outerShell_ccAnomalous=autoprocscalingstatistics['outerShell']['ccAnomalous'],
            outerShell_rMeasAllIPlusIMinus=autoprocscalingstatistics['outerShell']['rMeasAllIPlusIMinus'],

            overall_multiplicity=autoprocscalingstatistics['overall']['multiplicity'],
            overall_resolutionLimitLow=autoprocscalingstatistics['overall']['resolutionLimitLow'],
            overall_rMeasWithinIPlusIMinus=autoprocscalingstatistics['overall']['rMeasWithinIPlusIMinus'],
            overall_DanoOverSigDano=autoprocscalingstatistics['overall']['DanoOverSigDano'],
            overall_completenessSpherical=autoprocscalingstatistics['overall']['completenessSpherical'],
            overall_nTotalUniqueObservations=autoprocscalingstatistics['overall']['nTotalUniqueObservations'],
            overall_rMerge=autoprocscalingstatistics['overall']['rMerge'],
            overall_rPimWithinIPlusIMinus=autoprocscalingstatistics['overall']['rPimWithinIPlusIMinus'],
            overall_anomalousCompletenessEllipsoidal=autoprocscalingstatistics['overall']['anomalousCompletenessEllipsoidal'],
            overall_nTotalObservations=autoprocscalingstatistics['overall']['nTotalObservations'],
            overall_completenessEllipsoidal=autoprocscalingstatistics['overall']['completenessEllipsoidal'],
            overall_anomalousCompleteness=autoprocscalingstatistics['overall']['anomalousCompleteness'],
            overall_anomalousMultiplicity=autoprocscalingstatistics['overall']['anomalousMultiplicity'],
            overall_resolutionLimitHigh=autoprocscalingstatistics['overall']['resolutionLimitHigh'],
            overall_completeness=autoprocscalingstatistics['overall']['completeness'],
            overall_ccHalf=autoprocscalingstatistics['overall']['ccHalf'],
            overall_rPimAllIPlusIMinus=autoprocscalingstatistics['overall']['rPimAllIPlusIMinus'],
            overall_meanIOverSigI=autoprocscalingstatistics['overall']['meanIOverSigI'],
            overall_anomalousCompletenessSpherical=autoprocscalingstatistics['overall']['anomalousCompletenessSpherical'],
            overall_ccAnomalous=autoprocscalingstatistics['overall']['ccAnomalous'],
            overall_rMeasAllIPlusIMinus=autoprocscalingstatistics['overall']['rMeasAllIPlusIMinus'])

        report.save()
        return report.serialize
        logger.info("SAVED THE DATA")

    except:
        return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING REPORT"})

@csrf_exempt
def storeParseDPStep(data, report):

    """
    Creates nodes and relationships for each DPStep with relative properties
    """

    try:

            dpstep=DPStep()

            dpstep.save()
            dpstep2=dpstep.serialize
            
            connectReportDPStep(report['report_node_properties']['jobid'], dpstep2['dpstep_node_properties']['uuid'])
            
            return dpstep2

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING DPSTEP"})

@csrf_exempt
def storeParseAutoPROC(data, dpstep):

    """
    Creates nodes and relationships for autoPROC with relative properties
    """

    try:

            autroproc=autoPROC()

            autroproc.save()
            autroproc2=autroproc.serialize
            
            connectDPStepautoPROC(report['report_node_properties']['jobid'], autroproc2['autroproc_node_properties']['uuid'])
            
            return autroproc2

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING AUTOPROC"})

@csrf_exempt
def connectDPStepReport(data1, data2):
    
    """
    Create a relationship between a DPStep and a report node.
    """

    try:
        dpstep=DPStep.nodes.get(uuid=data1)
        report=Report.nodes.get(uuid=data2)
        return JsonResponse({"STATUS": dpstep.generates_report.connect(report)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING DPSTEP TO REPORT"}, safe=False)

@csrf_exempt
def connectDPStepautoPROC(data1, data2):
    
    """
    Create a relationship between a DPStep and an autoPROC node.
    """

    try:
        dpstep=DPStep.nodes.get(uuid=data1)
        autoproc=autoPROC.nodes.get(uuid=data2)
        return JsonResponse({"STATUS": dpstep.with_autoproc.connect(autoproc)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING DPSTEP TO AUTOPROC"}, safe=False)   