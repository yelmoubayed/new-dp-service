# Python imports
import json
import logging
import sys

# Django imports
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

# Models imports
from api.models.pipedream_model import GPhLPipedream
from api.models.report_model import Report
from api.models.dpstep_model import DPStep
from api.models.ligandsfitting_model import LigandsFitting
from api.models.reductionscaling_model import ReductionScaling
from api.models.refinement_cycles_model import RefinementCycles
from api.models.post_refinement_cycles_model import PostRefinementCycles
from api.models.ligand_model import Ligand
from api.models.ligand_statistics_model import LigandStatistics
from api.models.ligand_solutions_model import LigandSolutions
from api.models.reference_model import Reference
from api.models.structurefactors_model import StructureFactors
from api.models.mtzfile_model import MTZ

# Activate logger
logger=logging.getLogger('dataproc')
# logger.info('TEMPLATE')

@csrf_exempt
def storeProcInput(request):

    """
    Parse JSON data and call store functions for each model
    """

    if request.method=='POST':
        logger.info('ENTER API')
        json_data=json.loads(request.body)
        
        json_data_report=json_data

        json_data_ligand=json_data['ligandfitting']['ligands'][0]
        json_data_rc=json_data['refinement']['Cycles']
        json_data_prc=json_data['ligandfitting']['ligands'][0]['postrefinement'][0]
        json_data_solutions=json_data['ligandfitting']['ligands'][0]['solutions']
        json_data_statistics=json_data['ligandfitting']['ligands'][0]['validationstatistics']['ligandstatistics']
        
        # json_data_reference=json_data['dataprocessing']['referencedata']['cell']
        # json_data_reference=json_data['referencedata']['cell']

        # json_data_mtz=json_data['dataprocessing']

        try:
            
            report=storeParseReport(json_data_report)
            ligand=storeParseLigand(json_data_ligand, report)
            storeParseRC(json_data_rc, report)
            storeParsePRC(json_data_prc, ligand)
            storeParseLigandSolutions(json_data_solutions, ligand)
            storeParseLigandStatistics(json_data_statistics, ligand)
            # structurefactors=storeParseStructureFactors(json_data_reference)
            # storeParseStructureFactors(json_data_reference)


            return JsonResponse({"STATUS": "INPUT SUCCESSFULLY REGISTERED"})
        
        except :
            return JsonResponse({"STATUS":"ERROR OCCURRED"}, safe=False)

@csrf_exempt
def getOuterShell(autoprocscalingstatistics):
    result=[]
    for shell in autoprocscalingstatistics:
        if shell['scalingStatisticsType'] == "outerShell":
            result = shell
    return result


@csrf_exempt
def getInnerShell(autoprocscalingstatistics):
    result=[]
    for shell in autoprocscalingstatistics:
        if shell['scalingStatisticsType'] == "innerShell":
            result = shell
    return result


@csrf_exempt
def getOverall(autoprocscalingstatistics):
    result=[]
    for shell in autoprocscalingstatistics:
        if shell['scalingStatisticsType'] == "overall":
            result = shell
    return result

@csrf_exempt
def storeParseReport(data):

    """
    Creates nodes and relationships for each report with relative properties
    """

    logger.info("IN STORE PARSE report")
    try:        
        GPhL_pipedream=data['GPhL_pipedream']
        autoprocscaling=data['dataprocessing']['processingdata']['AutoProcscaling']
        autoprocscalingstatistics=data['dataprocessing']['processingdata']['AutoProcScalingStatistics']
        autoproc=data['dataprocessing']['processingdata']['AutoProc']

        innershell = getInnerShell(autoprocscalingstatistics)
        outershell = getOuterShell(autoprocscalingstatistics)
        overall = getOverall(autoprocscalingstatistics)
        logger.info("BEFORE REPORT")
        #logger.info(autoprocscalingstatistics['innerShell']['multiplicity'])
        report=Report(command=GPhL_pipedream['command'], 
            jsonversion=GPhL_pipedream['jsonversion'],
            runby=GPhL_pipedream['runby'],
            runfrom=GPhL_pipedream['runfrom'],
            jobid=GPhL_pipedream['jobid'],
            gphlpipedream_output=GPhL_pipedream['output'],
            version=GPhL_pipedream['version'],
            terminationstatus=GPhL_pipedream['terminationstatus'],

            refinedCell_beta=autoproc['refinedCell_beta'],
            refinedCell_b=autoproc['refinedCell_b'],
            wavelength=autoproc['wavelength'],
            refinedCell_a=autoproc['refinedCell_a'],
            refinedCell_alpha=autoproc['refinedCell_alpha'],
            spaceGroup=autoproc['spaceGroup'],
            refinedCell_c=autoproc['refinedCell_c'],
            refinedCell_gamma=autoproc['refinedCell_gamma'],

            recordTimeStamp=autoprocscaling['recordTimeStamp'],
            resolutionEllipsoidAxis13=autoprocscaling['resolutionEllipsoidAxis13'],
            resolutionEllipsoidAxis33=autoprocscaling['resolutionEllipsoidAxis33'],
            resolutionEllipsoidAxis23=autoprocscaling['resolutionEllipsoidAxis23'],
            resolutionEllipsoidValue2=autoprocscaling['resolutionEllipsoidValue2'],
            resolutionEllipsoidAxis21=autoprocscaling['resolutionEllipsoidAxis21'],
            resolutionEllipsoidAxis31=autoprocscaling['resolutionEllipsoidAxis31'],
            resolutionEllipsoidAxis22=autoprocscaling['resolutionEllipsoidAxis22'],
            resolutionEllipsoidAxis32=autoprocscaling['resolutionEllipsoidAxis32'],
            resolutionEllipsoidValue3=autoprocscaling['resolutionEllipsoidValue3'],
            resolutionEllipsoidAxis12=autoprocscaling['resolutionEllipsoidAxis12'],
            resolutionEllipsoidValue1=autoprocscaling['resolutionEllipsoidValue1'],
            resolutionEllipsoidAxis11=autoprocscaling['resolutionEllipsoidAxis11'],

            innerShell_multiplicity=innershell['multiplicity'],
            innerShell_resolutionLimitLow=innershell['resolutionLimitLow'],
            innerShell_rMeasWithinIPlusIMinus=innershell['rMeasWithinIPlusIMinus'],
            innerShell_DanoOverSigDano=innershell['DanoOverSigDano'],
            innerShell_completenessSpherical=innershell['completenessSpherical'],
            innerShell_nTotalUniqueObservations=innershell['nTotalUniqueObservations'],
            innerShell_rMerge=innershell['rMerge'],
            innerShell_rPimWithinIPlusIMinus=innershell['rPimWithinIPlusIMinus'],
            innerShell_anomalousCompletenessEllipsoidal=innershell['anomalousCompletenessEllipsoidal'],
            innerShell_nTotalObservations=innershell['nTotalObservations'],
            innerShell_completenessEllipsoidal=innershell['completenessEllipsoidal'],
            innerShell_anomalousCompleteness=innershell['anomalousCompleteness'],
            innerShell_anomalousMultiplicity=innershell['anomalousMultiplicity'],
            innerShell_resolutionLimitHigh=innershell['resolutionLimitHigh'],
            innerShell_completeness=innershell['completeness'],
            innerShell_ccHalf=innershell['ccHalf'],
            innerShell_rPimAllIPlusIMinus=innershell['rPimAllIPlusIMinus'],
            innerShell_meanIOverSigI=innershell['meanIOverSigI'],
            innerShell_anomalousCompletenessSpherical=innershell['anomalousCompletenessSpherical'],
            innerShell_ccAnomalous=innershell['ccAnomalous'],
            innerShell_rMeasAllIPlusIMinus=innershell['rMeasAllIPlusIMinus'],

            outerShell_multiplicity=outershell['multiplicity'],
            outerShell_resolutionLimitLow=outershell['resolutionLimitLow'],
            outerShell_rMeasWithinIPlusIMinus=outershell['rMeasWithinIPlusIMinus'],
            outerShell_DanoOverSigDano=outershell['DanoOverSigDano'],
            outerShell_completenessSpherical=outershell['completenessSpherical'],
            outerShell_nTotalUniqueObservations=outershell['nTotalUniqueObservations'],
            outerShell_rMerge=outershell['rMerge'],
            outerShell_rPimWithinIPlusIMinus=outershell['rPimWithinIPlusIMinus'],
            outerShell_anomalousCompletenessEllipsoidal=outershell['anomalousCompletenessEllipsoidal'],
            outerShell_nTotalObservations=outershell['nTotalObservations'],
            outerShell_completenessEllipsoidal=outershell['completenessEllipsoidal'],
            outerShell_anomalousCompleteness=outershell['anomalousCompleteness'],
            outerShell_anomalousMultiplicity=outershell['anomalousMultiplicity'],
            outerShell_resolutionLimitHigh=outershell['resolutionLimitHigh'],
            outerShell_completeness=outershell['completeness'],
            outerShell_ccHalf=outershell['ccHalf'],
            outerShell_rPimAllIPlusIMinus=outershell['rPimAllIPlusIMinus'],
            outerShell_meanIOverSigI=outershell['meanIOverSigI'],
            outerShell_anomalousCompletenessSpherical=outershell['anomalousCompletenessSpherical'],
            outerShell_ccAnomalous=outershell['ccAnomalous'],
            outerShell_rMeasAllIPlusIMinus=outershell['rMeasAllIPlusIMinus'],

            overall_multiplicity=overall['multiplicity'],
            overall_resolutionLimitLow=overall['resolutionLimitLow'],
            overall_rMeasWithinIPlusIMinus=overall['rMeasWithinIPlusIMinus'],
            overall_DanoOverSigDano=overall['DanoOverSigDano'],
            overall_completenessSpherical=overall['completenessSpherical'],
            overall_nTotalUniqueObservations=overall['nTotalUniqueObservations'],
            overall_rMerge=overall['rMerge'],
            overall_rPimWithinIPlusIMinus=overall['rPimWithinIPlusIMinus'],
            overall_anomalousCompletenessEllipsoidal=overall['anomalousCompletenessEllipsoidal'],
            overall_nTotalObservations=overall['nTotalObservations'],
            overall_completenessEllipsoidal=overall['completenessEllipsoidal'],
            overall_anomalousCompleteness=overall['anomalousCompleteness'],
            overall_anomalousMultiplicity=overall['anomalousMultiplicity'],
            overall_resolutionLimitHigh=overall['resolutionLimitHigh'],
            overall_completeness=overall['completeness'],
            overall_ccHalf=overall['ccHalf'],
            overall_rPimAllIPlusIMinus=overall['rPimAllIPlusIMinus'],
            overall_meanIOverSigI=overall['meanIOverSigI'],
            overall_anomalousCompletenessSpherical=overall['anomalousCompletenessSpherical'],
            overall_ccAnomalous=overall['ccAnomalous'],
            overall_rMeasAllIPlusIMinus=overall['rMeasAllIPlusIMinus'])

        report.save()
        return report.serialize

    except:
        return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING REPORT"})

@csrf_exempt
def storeParseReference():

    """
    Creates nodes for reference label
    """

    try:
        reference=Reference()
        reference.save()
        reference2=reference.serialize
        
        return reference2

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING REFERENCE LABEL NODE"})

@csrf_exempt
def storeParseStructureFactors(data, reference):

    """
    Creates nodes for Structure factors with relative properties
    """

    try:
        structurefactors=StructureFactors(
            gamma=data['cell']['gamma'],
            a=data['cell']['a'],         
            alpha=data['cell']['alpha'],
            b=data['cell']['b'],
            beta=data['cell']['beta'],
            c=data['cell']['c'],
            symmetry=data['symmetry'])

        structurefactors.save()
        structurefactors2=structurefactors.serialize 

        connectStructureFactorsRef(structurefactors2['structurefactors_node_properties']['uuid'], reference['ref_node_properties']['uuid'])
        
        return ({"STATUS": "STRUCTURE FACTORS REGISTERED"})

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING RC"})

@csrf_exempt
def storeParseDPStep():

    """
    Creates nodes for dpstep label
    """

    try:
        dpstep=DPStep()
        dpstep.save()
        dpstep2=dpstep.serialize
        
        return dpstep2

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING DPSTEP LABEL NODE"})

@csrf_exempt
def storeParseRC(data, report):

    """
    Creates nodes and relationships for each RC (Refinement Cycle) with relative properties
    """
    
    try:
        for input_rc in data:
            rc=RefinementCycles(cycle_number=input_rc['cycle_number'],
                WilsonB=input_rc['WilsonB'],
                Rfree=input_rc['Rfree'],
                MeanB=input_rc['MeanB'],
                rc_type=input_rc['type'],
                step=input_rc['step'],
                RMSbonds=input_rc['RMSbonds'],
                RMSangles=input_rc['RMSangles'],
                R=input_rc['R'],
                WatersPresent=input_rc['WatersPresent'])
           
            rc.save()
            rc2=rc.serialize
            connectReportRC(report['report_node_properties']['jobid'], rc2['rc_node_properties']['uuid'])

        return ({"STATUS": "RC REGISTERED"})

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING RC"})

@csrf_exempt
def storeParseLigand(data, report):

    """
    Creates nodes and relationships for each ligand with relative properties
    """

    try:
            molprobity=data['validationstatistics']['molprobity']

            ligand=Ligand(ligandname=data['ligandname'],
                molprobitypercentile=molprobity['molprobitypercentile'],
                ramaoutlierpercent=molprobity['ramaoutlierpercent'],
                cbetadeviations=molprobity['cbetadeviations'],
                ramafavoredpercent=molprobity['ramafavoredpercent'],
                poorrotamers=molprobity['poorrotamers'],
                rmsbonds=molprobity['rmsbonds'],
                rmsangles=molprobity['rmsangles'],
                clashpercentile=molprobity['clashpercentile'],
                poorrotamerspercent=molprobity['poorrotamerspercent'],
                clashscore=molprobity['clashscore'],
                ramafavored=molprobity['ramafavored'],
                molprobityscore=molprobity['molprobityscore'],
                ramaoutliers=molprobity['ramaoutliers'])

            ligand.save()
            ligand2=ligand.serialize
            
            connectReportLigand(report['report_node_properties']['jobid'], ligand2['ligand_node_properties']['uuid'])
            
            return ligand2

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING LIGAND"})

@csrf_exempt
def storeParsePRC(data, ligand):

    """
    Creates nodes and relationships for each PRC (Post-Refinement Cycle) with relative properties
    """

    try:
        for input_prc in data:
            prc=PostRefinementCycles(cycle_number=input_prc['cycle_number'],
                WilsonB=input_prc['WilsonB'],
                Rfree=input_prc['Rfree'],
                MeanB=input_prc['MeanB'],
                prc_type=input_prc['type'],
                step=input_prc['step'],
                RMSbonds=input_prc['RMSbonds'],
                RMSangles=input_prc['RMSangles'],
                R=input_prc['R'],
                WatersPresent=input_prc['WatersPresent'])
            prc.save()
            prc2=prc.serialize
            connectLigandPRC(prc2['prc_node_properties']['uuid'], ligand['ligand_node_properties']['uuid'])

        return ({"STATUS": "PRC REGISTERED"})

    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING PRC"})

@csrf_exempt
def storeParseLigandSolutions(data, ligand):

    """
    Creates nodes and relationships for each ligand solutions with relative properties
    """

    try:
        for input_solutions in data:

            solutions=LigandSolutions(closecontacts=input_solutions['closecontacts'],
                chain=input_solutions['chain'],
                contactscore=input_solutions['contactscore'],
                ligandstrain=input_solutions['ligandstrain'],
                poorfit=input_solutions['poorfit'],
                output=input_solutions['output'],
                correlationcoefficient=input_solutions['correlationcoefficient'],
                rhofitscore=input_solutions['rhofitscore'],
                solution_number=input_solutions['solution_number'])
                # WatersPresent=input_rc['WatersPresent'])
            solutions.save()
            solutions2=solutions.serialize
            connectLigandLigandSolutions(ligand['ligand_node_properties']['uuid'], solutions2['ligand_solutions_node_properties']['uuid'])

        return ({"STATUS": "LIGANDS SOLUTIONS REGISTERED"})
    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING LIGANDS SOLUTIONS"})

@csrf_exempt
def storeParseLigandStatistics(data, ligand):

    """
    Creates nodes and relationships for each ligand solutions with relative properties
    """

    try:
        for input_statistics in data:

            statistics=LigandStatistics(ligandomin=input_statistics['ligandomin'],
                mogulzbond=input_statistics['mogulzbond'],
                ligandbmin=input_statistics['ligandbmin'],
                mogulring=input_statistics['mogulring'],
                moguldihe=input_statistics['moguldihe'],
                ligandbavg=input_statistics['ligandbavg'],
                mogulangl=input_statistics['mogulangl'],
                mogulbond=input_statistics['mogulbond'],
                ligandbmax=input_statistics['ligandbmax'],
                ligandid=input_statistics['ligandid'],
                ligandomax=input_statistics['ligandomax'],
                ligandcc=input_statistics['ligandcc'],
                mogulzangl=input_statistics['mogulzangl'])

            statistics.save()
            statistics2=statistics.serialize
            connectLigandLigandStatistics(ligand['ligand_node_properties']['uuid'], statistics2['ligand_statistics_node_properties']['uuid'])

        return ({"STATUS": "LIGANDS SOLUTIONS REGISTERED"})
    except:
            return ({"STATUS": "ERROR OCCURRED WHILE REGISTERING LIGANDS SOLUTIONS"})

@csrf_exempt
def connectReportLigand(data1, data2):

    """
    Create a relationship between a report and a ligand
    """

    try:

        report=Report.nodes.get(jobid=data1)
        ligand=Ligand.nodes.get(uuid=data2)
        return JsonResponse({"STATUS": report.has_ligand.connect(ligand)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING REPORT TO LIGAND"}, safe=False)

@csrf_exempt
def connectReportRC(data1, data2):
    
    """
    Create a relationship between a report and a rc
    """

    try:
        report=Report.nodes.get(jobid=data1)
        rc=RefinementCycles.nodes.get(uuid=data2)
        return JsonResponse({"STATUS": report.has_refinement_cycles.connect(rc)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING REPORT TO RCs"}, safe=False)

@csrf_exempt
def connectLigandLigandSolutions(data1, data2):
    
    """
    Create a relationship between a ligand and ligand solutions
    """

    try:
        ligand=Ligand.nodes.get(uuid=data1)
        solutions=LigandSolutions.nodes.get(uuid=data2)

        return JsonResponse({"STATUS": ligand.has_solutions.connect(solutions)}, safe=False)
    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING LIGAND TO SOLUTIONS"}, safe=False)

@csrf_exempt
def connectLigandLigandStatistics(data1, data2):
    
    """
    Create a relationship between a ligand and ligand statistics
    """

    try:
        ligand=Ligand.nodes.get(uuid=data1)
        statistics=LigandStatistics.nodes.get(uuid=data2)
        return JsonResponse({"STATUS": ligand.has_statistics.connect(statistics)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING LIGAND TO PRCs"}, safe=False)

@csrf_exempt
def connectLigandPRC(data1, data2):
    
    """
    Create a relationship between a report and a prc
    """

    try:
        ligand=Ligand.nodes.get(uuid=data2)
        prc=PostRefinementCycles.nodes.get(uuid=data1)
        return JsonResponse({"STATUS": ligand.has_postrefinement_cycles.connect(prc)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING LIGAND TO PRCs"}, safe=False)

@csrf_exempt
def connectStructureFactorsRef(data1, data2):
    
    """
    Create a relationship between a SF (Structure factors) and Reference node.
    """
    logger.info('ENTER FUNCTION')

    try:

        logger.info('TRY FUNCTION')
        structurefactors=StructureFactors.nodes.get(uuid=data1)
        ref=Reference.nodes.get(uuid=data2)

        logger.info(structurefactors)
        logger.info(ref)

        return JsonResponse({"STATUS": structurefactors.labelled_ref.connect(ref)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING STRUCTURE FACTORS TO REFERENCE"}, safe=False)

@csrf_exempt
def connectStructureFactorsDPStep(data1, data2):
    
    """
    Create a relationship between a SF (Structure factors) and MTZ node.
    """

    try:
        structurefactors=StructureFactors.nodes.get(uuid=data1)
        dpstep=DPStep.nodes.get(uuid=data2)
        return JsonResponse({"STATUS": structurefactors.input_of.connect(dpstep)}, safe=False)

    except:
        return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING STRUCTURE FACTORS TO DPSTEP"}, safe=False)

# @csrf_exempt
# def connectStructureFactorsMTZ(data1, data2):
    
#     """
#     Create a relationship between a SF (Structure factors) and MTZ node.
#     """

#     try:
#         structurefactors=StructureFactors.nodes.get(uuid=data1)
#         mtz=MTZ.nodes.get(uuid=data2)
#         return JsonResponse({"STATUS": structurefactors.is_mtz.connect(mtz)}, safe=False)

#     except:
#         return JsonResponse({"STATUS": "ERROR OCCURRED WHILE CONNECTING STRUCTURE FACTORS TO MTZ"}, safe=False)
