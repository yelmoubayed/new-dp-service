# Python imports
from uuid import uuid4

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports

class LigandStatistics(StructuredNode):
	
	'''
	Defines node properties and relationships
	Provides data serializer
	'''

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	ligandomin=StringProperty()
	mogulzbond=StringProperty()
	ligandbmin=StringProperty()
	mogulring=StringProperty()
	moguldihe=StringProperty()
	ligandbavg=StringProperty()
	mogulangl=StringProperty()
	mogulbond=StringProperty()
	ligandbmax=StringProperty()
	ligandid=StringProperty()
	ligandomax=StringProperty()
	ligandcc=StringProperty()
	mogulzangl=StringProperty()
	ligandresno=StringProperty()

	@property
	def serialize(self):

		'''
		Serializer for node properties
		'''
		
		return {
		'ligand_statistics_node_properties': {

		"uuid": self.uuid,
		'ligandomin': self.ligandomin,
		'mogulzbond': self.mogulzbond,
		'ligandbmin': self.ligandbmin,
		'mogulring': self.mogulring,
		'moguldihe': self.moguldihe,
		'ligandbavg': self.ligandbavg,
		'mogulangl': self.mogulangl,
		'mogulbond': self.mogulbond,
		'ligandbmax': self.ligandbmax,
		'ligandid': self.ligandid,
		'ligandomax': self.ligandomax,
		'ligandcc': self.ligandcc,
		'mogulzangl': self.mogulzangl,
		'ligandresno': self.ligandresno,
		},
		}