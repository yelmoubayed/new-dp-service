# Python imports
from uuid import uuid4

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports

class RefinementCycles(StructuredNode):
	
	'''
	Defines node properties and relationships
	Provides data serializer
	'''

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	cycle_number=IntegerProperty()
	WilsonB=IntegerProperty()
	Rfree=IntegerProperty()
	MeanB=IntegerProperty()
	rc_type=StringProperty()
	step=StringProperty()
	RMSbonds=IntegerProperty()
	RMSangles=IntegerProperty()
	R=IntegerProperty()
	WatersPresent=IntegerProperty()
	number=IntegerProperty()
	LMRscore=StringProperty()
	modelname=StringProperty()
	selectedmodel=StringProperty()
	refinementprotocol=StringProperty()
	transcis=IntegerProperty()
	hbondflip=IntegerProperty()
	pepflip=StringProperty()
	cistrans=IntegerProperty()
	changedrot=IntegerProperty()
	watersremoved=IntegerProperty()

	@property
	def serialize(self):

		'''
		Serializer for node properties
		'''
		
		return {
		'rc_node_properties': {
		
		'uuid': self.uuid,
		'WilsonB': self.WilsonB,
		'Rfree': self.Rfree,
		'MeanB': self.MeanB,
		'rc_type': self.rc_type,
		'step': self.step,
		'RMSbonds': self.RMSbonds,
		'RMSangles': self.RMSangles,
		'R': self.R,
		'WatersPresent': self.WatersPresent,
		'cycle_number': self.cycle_number,
		'number': self.number,
		'LMRscore': self.LMRscore,
		'modelname': self.modelname,
		'selectedmodel': self.selectedmodel,
		'refinementprotocol': self.refinementprotocol,
		'transcis':self.transcis,
		'hbondflip':self.hbondflip,
		'pepflip':self.pepflip,
		'cistrans':self.cistrans,
		'changedrot':self.changedrot,
		'watersremoved':self.watersremoved,
		},
		}