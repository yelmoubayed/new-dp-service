# Python imports
from uuid import uuid4

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports

class LigandSolutions(StructuredNode):
	
	'''
	Defines node properties and relationships
	Provides data serializer
	'''

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	closecontacts=StringProperty()
	chain=StringProperty()
	contactscore=IntegerProperty()
	ligandstrain=IntegerProperty()
	poorfit=StringProperty()
	output=StringProperty()
	correlationcoefficient=IntegerProperty()
	rhofitscore=IntegerProperty()
	solution_number=IntegerProperty()

	@property
	def serialize(self):

		'''
		Serializer for node properties
		'''
		
		return {
		'ligand_solutions_node_properties': {

		"uuid": self.uuid,
		"closecontacts": self.closecontacts,
		"rhofitscore": self.rhofitscore,
		"chain": self.chain,
		"correlationcoefficient": self.correlationcoefficient,
		"output": self.output,
		"ligandstrain": self.ligandstrain,
		"poorfit": self.poorfit,
		"contactscore": self.contactscore,
		"solution_number": self.solution_number,
		},
		}
		


