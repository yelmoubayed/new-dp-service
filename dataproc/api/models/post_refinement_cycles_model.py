# Python imports
from uuid import uuid4

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports

class PostRefinementCycles(StructuredNode):
	
	'''
	Defines node properties and relationships
	Provides data serializer
	'''

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	cycle_number=IntegerProperty()
	WilsonB=IntegerProperty()
	Rfree=IntegerProperty()
	MeanB=IntegerProperty()
	prc_type=StringProperty()
	step=StringProperty()
	RMSbonds=IntegerProperty()
	RMSangles=IntegerProperty()
	R=IntegerProperty()
	WatersPresent=IntegerProperty()
	selectedmodel=StringProperty()
	
	@property
	def serialize(self):

		'''
		Serializer for node properties
		'''
		
		return {
		'prc_node_properties': {
		
		'uuid': self.uuid,
		'WilsonB': self.WilsonB,
		'Rfree': self.Rfree,
		'MeanB': self.MeanB,
		'prc_type': self.prc_type,
		'step': self.step,
		'RMSbonds': self.RMSbonds,
		'RMSangles': self.RMSangles,
		'R': self.R,
		'WatersPresent': self.WatersPresent,
		'cycle_number': self.cycle_number,
		'selectedmodel': self.selectedmodel,

		},
		}