# Python imports
from uuid import uuid4
import logging

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, FloatProperty, DateTimeProperty, DateProperty, RelationshipTo

# Models imports
from api.models.statisticalreport_model import StatisticalReport
from api.models.summaryout_model import SummaryOut
from api.models.summaryhtml_model import SummaryHtml
from api.models.post_refinement_cycles_model import PostRefinementCycles
from api.models.refinement_cycles_model import RefinementCycles
from api.models.ligand_model import Ligand
from api.models.report_file_model import ReportFile

class Report(StructuredNode):
	
	'''
	Defines node properties and relationships
	Provides data serializer
	'''

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	
		# GPhL_pipedream report properties
	command=StringProperty(max_length=700)
	jsonversion=StringProperty(max_length=700)
	runby=StringProperty(max_length=700)
	runfrom=StringProperty(max_length=700)
	jobid=StringProperty(unique_index=True, default=uuid4)
	gphlpipedream_output=StringProperty(max_length=700)
	version=StringProperty(max_length=700)
	terminationstatus=StringProperty(max_length=700)
	
		# AutoProcscaling report properties
	recordTimeStamp=StringProperty()
	resolutionEllipsoidAxis13=FloatProperty()
	resolutionEllipsoidAxis33=FloatProperty()
	resolutionEllipsoidAxis23=FloatProperty()
	resolutionEllipsoidValue2=FloatProperty()
	resolutionEllipsoidAxis21=FloatProperty()
	resolutionEllipsoidAxis31=FloatProperty()
	resolutionEllipsoidAxis22=FloatProperty()
	resolutionEllipsoidAxis32=FloatProperty()
	resolutionEllipsoidValue3=FloatProperty()
	resolutionEllipsoidAxis12=FloatProperty()
	resolutionEllipsoidValue1=FloatProperty()
	resolutionEllipsoidAxis11=FloatProperty()

 		# AutoProcScalingStatistics report properties (outerShell, innerShell, and overall)
	outerShell_multiplicity=StringProperty()
	outerShell_resolutionLimitLow=StringProperty()
	outerShell_rMeasWithinIPlusIMinus=StringProperty()
	outerShell_DanoOverSigDano=StringProperty()
	outerShell_completenessSpherical=StringProperty()
	outerShell_nTotalUniqueObservations=StringProperty()
	outerShell_rMerge=StringProperty()
	outerShell_rPimWithinIPlusIMinus=StringProperty()
	outerShell_anomalousCompletenessEllipsoidal=StringProperty()
	outerShell_nTotalObservations=StringProperty()
	outerShell_completenessEllipsoidal=StringProperty()
	outerShell_anomalousCompleteness=StringProperty()
	outerShell_anomalousMultiplicity=StringProperty()
	outerShell_resolutionLimitHigh=StringProperty()
	outerShell_completeness=StringProperty()
	outerShell_ccHalf=StringProperty()
	outerShell_rPimAllIPlusIMinus=StringProperty()
	outerShell_meanIOverSigI=StringProperty()
	outerShell_anomalousCompletenessSpherical=StringProperty()
	outerShell_ccAnomalous=StringProperty()
	outerShell_rMeasAllIPlusIMinus=StringProperty()

	innerShell_multiplicity=StringProperty()
	innerShell_resolutionLimitLow=StringProperty()
	innerShell_rMeasWithinIPlusIMinus=StringProperty()
	innerShell_DanoOverSigDano=StringProperty()
	innerShell_completenessSpherical=StringProperty()
	innerShell_nTotalUniqueObservations=StringProperty()
	innerShell_rMerge=StringProperty()
	innerShell_rPimWithinIPlusIMinus=StringProperty()
	innerShell_anomalousCompletenessEllipsoidal=StringProperty()
	innerShell_nTotalObservations=StringProperty()
	innerShell_completenessEllipsoidal=StringProperty()
	innerShell_anomalousCompleteness=StringProperty()
	innerShell_anomalousMultiplicity=StringProperty()
	innerShell_resolutionLimitHigh=StringProperty()
	innerShell_completeness=StringProperty()
	innerShell_ccHalf=StringProperty()
	innerShell_rPimAllIPlusIMinus=StringProperty()
	innerShell_meanIOverSigI=StringProperty()
	innerShell_anomalousCompletenessSpherical=StringProperty()
	innerShell_ccAnomalous=StringProperty()
	innerShell_rMeasAllIPlusIMinus=StringProperty()

	overall_multiplicity=StringProperty()
	overall_resolutionLimitLow=StringProperty()
	overall_rMeasWithinIPlusIMinus=StringProperty()
	overall_DanoOverSigDano=StringProperty()
	overall_completenessSpherical=StringProperty()
	overall_nTotalUniqueObservations=StringProperty()
	overall_rMerge=StringProperty()
	overall_rPimWithinIPlusIMinus=StringProperty()
	overall_anomalousCompletenessEllipsoidal=StringProperty()
	overall_nTotalObservations=StringProperty()
	overall_completenessEllipsoidal=StringProperty()
	overall_anomalousCompleteness=StringProperty()
	overall_anomalousMultiplicity=StringProperty()
	overall_resolutionLimitHigh=StringProperty()
	overall_completeness=StringProperty()
	overall_ccHalf=StringProperty()
	overall_rPimAllIPlusIMinus=StringProperty()
	overall_meanIOverSigI=StringProperty()
	overall_anomalousCompletenessSpherical=StringProperty()
	overall_ccAnomalous=StringProperty()
	overall_rMeasAllIPlusIMinus=StringProperty()

		# AutoProc
	refinedCell_beta=StringProperty()
	refinedCell_b=StringProperty()
	wavelength=StringProperty()
	refinedCell_a=StringProperty()
	refinedCell_alpha=StringProperty()
	spaceGroup=StringProperty()
	refinedCell_c=StringProperty()
	refinedCell_gamma=StringProperty()

	# Relationships
	has_postrefinement_cycles=RelationshipTo(PostRefinementCycles, 'HAS')
	has_refinement_cycles=RelationshipTo(RefinementCycles, 'HAS')
	has_ligand=RelationshipTo(Ligand, 'HAS')
	has_reportfile=RelationshipTo(ReportFile, 'HAS')

	@property
	def serialize(self):

		'''
		Serializer for node properties
		'''

		return {
		'report_node_properties': {

		'command': self.command,
		'jsonversion': self.jsonversion,
		'runby': self.runby,
		'runfrom': self.runfrom,
		'jobid': self.jobid,
		'gphlpipedream_output': self.gphlpipedream_output,
		'version': self.version,
		'terminationstatus': self.terminationstatus,

		'refinedCell_beta': self.refinedCell_beta,
		'refinedCell_b': self.refinedCell_b,
		'wavelength': self.wavelength,
		'refinedCell_a': self.refinedCell_a,
		'refinedCell_alpha': self.refinedCell_alpha,
		'spaceGroup': self.spaceGroup,
		'refinedCell_c': self.refinedCell_c,
		'refinedCell_gamma': self.refinedCell_gamma,
		
		'recordTimeStamp': self.recordTimeStamp,
		'resolutionEllipsoidAxis13': self.resolutionEllipsoidAxis13,
		'resolutionEllipsoidAxis33': self.resolutionEllipsoidAxis33,
		'resolutionEllipsoidAxis23': self.resolutionEllipsoidAxis23,
		'resolutionEllipsoidValue2': self.resolutionEllipsoidValue2,
		'resolutionEllipsoidAxis21': self.resolutionEllipsoidAxis21,
		'resolutionEllipsoidAxis31': self.resolutionEllipsoidAxis31,
		'resolutionEllipsoidAxis22': self.resolutionEllipsoidAxis22,
		'resolutionEllipsoidAxis32': self.resolutionEllipsoidAxis32,
		'resolutionEllipsoidValue3': self.resolutionEllipsoidValue3,
		'resolutionEllipsoidAxis12': self.resolutionEllipsoidAxis12,
		'resolutionEllipsoidValue1': self.resolutionEllipsoidValue1,
		'resolutionEllipsoidAxis11': self.resolutionEllipsoidAxis11,

		'overall_multiplicity': self.overall_multiplicity,
		'overall_resolutionLimitLow': self.overall_resolutionLimitLow,
		'overall_rMeasWithinIPlusIMinus': self.overall_rMeasWithinIPlusIMinus,
		'overall_DanoOverSigDano': self.overall_DanoOverSigDano,
		'overall_completenessSpherical': self.overall_completenessSpherical,
		'overall_nTotalUniqueObservations': self.overall_nTotalUniqueObservations,
		'overall_rMerge': self.overall_rMerge,
		'overall_rPimWithinIPlusIMinus': self.overall_rPimWithinIPlusIMinus,
		'overall_anomalousCompletenessEllipsoidal': self.overall_anomalousCompletenessEllipsoidal,
		'overall_nTotalObservations': self.overall_nTotalObservations,
		'overall_completenessEllipsoidal': self.overall_completenessEllipsoidal,
		'overall_anomalousCompleteness': self.overall_anomalousCompleteness,
		'overall_anomalousMultiplicity': self.overall_anomalousMultiplicity,
		'overall_resolutionLimitHigh': self.overall_resolutionLimitHigh,
		'overall_completeness': self.overall_completeness,
		'overall_ccHalf': self.overall_ccHalf,
		'overall_rPimAllIPlusIMinus': self.overall_rPimAllIPlusIMinus,
		'overall_meanIOverSigI': self.overall_meanIOverSigI,
		'overall_anomalousCompletenessSpherical': self.overall_anomalousCompletenessSpherical,
		'overall_ccAnomalous': self.overall_ccAnomalous,
		'overall_rMeasAllIPlusIMinus': self.overall_rMeasAllIPlusIMinus,

		'outerShell_multiplicity': self.outerShell_multiplicity,
		'outerShell_resolutionLimitLow': self.outerShell_resolutionLimitLow,
		'outerShell_rMeasWithinIPlusIMinus': self.outerShell_rMeasWithinIPlusIMinus,
		'outerShell_DanoOverSigDano': self.outerShell_DanoOverSigDano,
		'outerShell_completenessSpherical': self.outerShell_completenessSpherical,
		'outerShell_nTotalUniqueObservations': self.outerShell_nTotalUniqueObservations,
		'outerShell_rMerge': self.outerShell_rMerge,
		'outerShell_rPimWithinIPlusIMinus': self.outerShell_rPimWithinIPlusIMinus,
		'outerShell_anomalousCompletenessEllipsoidal': self.outerShell_anomalousCompletenessEllipsoidal,
		'outerShell_nTotalObservations': self.outerShell_nTotalObservations,
		'outerShell_completenessEllipsoidal': self.outerShell_completenessEllipsoidal,
		'outerShell_anomalousCompleteness': self.outerShell_anomalousCompleteness,
		'outerShell_anomalousMultiplicity': self.outerShell_anomalousMultiplicity,
		'outerShell_resolutionLimitHigh': self.outerShell_resolutionLimitHigh,
		'outerShell_completeness': self.outerShell_completeness,
		'outerShell_ccHalf': self.outerShell_ccHalf,
		'outerShell_rPimAllIPlusIMinus': self.outerShell_rPimAllIPlusIMinus,
		'outerShell_meanIOverSigI': self.outerShell_meanIOverSigI,
		'outerShell_anomalousCompletenessSpherical': self.outerShell_anomalousCompletenessSpherical,
		'outerShell_ccAnomalous': self.outerShell_ccAnomalous,
		'outerShell_rMeasAllIPlusIMinus': self.outerShell_rMeasAllIPlusIMinus,

		'innerShell_multiplicity': self.innerShell_multiplicity,
		'innerShell_resolutionLimitLow': self.innerShell_resolutionLimitLow,
		'innerShell_rMeasWithinIPlusIMinus': self.innerShell_rMeasWithinIPlusIMinus,
		'innerShell_DanoOverSigDano': self.innerShell_DanoOverSigDano,
		'innerShell_completenessSpherical': self.innerShell_completenessSpherical,
		'innerShell_nTotalUniqueObservations': self.innerShell_nTotalUniqueObservations,
		'innerShell_rMerge': self.innerShell_rMerge,
		'innerShell_rPimWithinIPlusIMinus': self.innerShell_rPimWithinIPlusIMinus,
		'innerShell_anomalousCompletenessEllipsoidal': self.innerShell_anomalousCompletenessEllipsoidal,
		'innerShell_nTotalObservations': self.innerShell_nTotalObservations,
		'innerShell_completenessEllipsoidal': self.innerShell_completenessEllipsoidal,
		'innerShell_anomalousCompleteness': self.innerShell_anomalousCompleteness,
		'innerShell_anomalousMultiplicity': self.innerShell_anomalousMultiplicity,
		'innerShell_resolutionLimitHigh': self.innerShell_resolutionLimitHigh,
		'innerShell_completeness': self.innerShell_completeness,
		'innerShell_ccHalf': self.innerShell_ccHalf,
		'innerShell_rPimAllIPlusIMinus': self.innerShell_rPimAllIPlusIMinus,
		'innerShell_meanIOverSigI': self.innerShell_meanIOverSigI,
		'innerShell_anomalousCompletenessSpherical': self.innerShell_anomalousCompletenessSpherical,
		'innerShell_ccAnomalous': self.innerShell_ccAnomalous,
		'innerShell_rMeasAllIPlusIMinus': self.innerShell_rMeasAllIPlusIMinus,
		},
		}
