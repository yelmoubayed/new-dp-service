# Python imports
from uuid import uuid4

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports
from api.models.dpstep_model import DPStep
# from api.models.mtzfile_model import MTZ
# from api.models.scalepackfile_model import ScalepackFile
from api.models.reference_model import Reference
# from api.models.construct_model import Construct

class StructureFactors(StructuredNode):
	
	"""
	Defines node properties and relationships
	Provides data serializer
	"""

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	gamma=StringProperty()
	a=StringProperty()
	alpha=StringProperty()
	b=StringProperty()
	beta=StringProperty()
	c=StringProperty()
	symmetry=StringProperty()

	# Relationships
	# is_scalepack=RelationshipTo('ScalepackFile', 'IS')
	# is_mtz=RelationshipTo('MTZfile', 'IS')
	labelled_ref=RelationshipTo(Reference, 'LABELLED_AS')
	# input_as_ref=RelationshipTo('DPStep', 'INPUT_AS_REFERENCE')
	input_of=RelationshipTo('DPStep', 'INPUT')
	# belongs=RelationshipTo('Construct', 'BELONGS')

	@property
	def serialize(self):

		"""
		Serializer for node properties
		"""
		
		return {
		'structurefactors_node_properties': {
		'uuid': self.uuid,
		'gamma': self.gamma,
		'a': self.a,
		'b': self.b,
		'beta': self.beta,
		'c': self.c,
		'symmetry': self.symmetry,
		'alpha': self.alpha,
		},
		}

