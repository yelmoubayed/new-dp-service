# Python imports
from uuid import uuid4
import logging

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports
from api.models.statisticalreport_model import StatisticalReport
from api.models.summaryout_model import SummaryOut
from api.models.summaryhtml_model import SummaryHtml

# Activate logger
logger=logging.getLogger('dataproc')

class ReportFile(StructuredNode):
	
	'''
	Defines node properties and relationships
	Provides data serializer
	'''

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	filesize=StringProperty()
	filepath=StringProperty()

	# Relationships 
	is_statistical=RelationshipTo(StatisticalReport, 'IS')
	is_summaryout=RelationshipTo(SummaryOut, 'IS')
	is_html=RelationshipTo(SummaryHtml, 'IS')

	@property
	def serialize(self):

		'''
		Serializer for node properties
		'''

		return {
		'reportfile_node_properties': {
		'uuid': self.uuid,
		'filepath': self.filepath,
		'filesize': self.filesize,	
		},
		}