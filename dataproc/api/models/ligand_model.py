# Python imports
from uuid import uuid4

# Third-party imports
from neomodel import StructuredNode, StringProperty, IntegerProperty, UniqueIdProperty, RelationshipTo

# Models imports
# from api.models.dataset_model import Dataset
from api.models.ligand_solutions_model import LigandSolutions
from api.models.ligand_statistics_model import LigandStatistics
from api.models.post_refinement_cycles_model import PostRefinementCycles


class Ligand(StructuredNode):

	"""
	Defines node properties and relationships
	Provides data serializer
	"""

	# Properties
	uuid=StringProperty(unique_index=True, default=uuid4)
	ligandname=StringProperty()

	# Ligands fitting (Rhofit) report properties
	molprobitypercentile=StringProperty()
	ramaoutlierpercent=StringProperty()
	cbetadeviations=StringProperty()
	ramafavoredpercent=StringProperty()
	poorrotamers=IntegerProperty()
	rmsbonds=IntegerProperty()
	rmsangles=IntegerProperty()
	clashpercentile=StringProperty()
	poorrotamerspercent=IntegerProperty()
	clashscore=IntegerProperty()
	ramafavored=StringProperty()
	molprobityscore=IntegerProperty()
	ramaoutliers=StringProperty()

	# Relationships
	has_solutions=RelationshipTo(LigandSolutions, 'HAS')
	has_statistics=RelationshipTo(LigandStatistics, 'HAS')
	has_postrefinement_cycles=RelationshipTo(PostRefinementCycles, 'HAS')

	@property
	def serialize(self):

		"""
		Serializer for node properties
		"""
		
		return {

		'ligand_node_properties': {
		'uuid': self.uuid,
		'ligandname': self.ligandname,
		'molprobitypercentile': self.molprobitypercentile,
		'ramaoutlierpercent': self.ramaoutlierpercent,
		'cbetadeviations': self.cbetadeviations,
		'ramafavoredpercent': self.ramafavoredpercent,
		'poorrotamers': self.poorrotamers,
		'rmsbonds': self.rmsbonds,
		'rmsangles': self.rmsangles,
		'clashpercentile': self.clashpercentile,
		'poorrotamerspercent': self.poorrotamerspercent,
		'clashscore': self.clashscore,
		'ramafavored': self.ramafavored,
		'molprobityscore': self.molprobityscore,
		'ramaoutliers': self.ramaoutliers,
		},
		}
